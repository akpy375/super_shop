# The test project

## Launching the project in Docker
- ```docker compose up -d --build```
---

## For local startup, use:

|    command    | description   |
|:--------------|:--------------|
| ```git clone https://gitlab.com/akpy375/super_shop.git``` |  Копируем проект        |
| ```cd super_shop```         | Заходим в папку с проектом        |
| ```python3 -m venv venv```         | Создаем рабочее окружение        |
| ```source venv/bin/activate```         | Подкл. к рабочему окружению        |
| ```pip install -r requirements.txt```       | Установка зависимостей        |
| ```python3 manage.py migrate```       | Создание миграций        |
| ```python3 manage.py loaddata db.json```       | Заполнение БД тестовыми данными из JSON        |
| ```python3 manage.py runserver```       | Запуск проекта        |
| ```pre-commit install```       | Установка pre-commit хуков        |
| ```pre-commit```       | Ручная проверка проблем  |


## Endpoints

- ```http://127.0.0.1:8000/swagger/```

![User URL](docs/pictures/swagger.png)
