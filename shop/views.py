from rest_framework import generics, status
from rest_framework.response import Response

from shop.models import Manufacturer
from shop.serializers import ManufacturerSerializer
from shop.service import ShopService


class ManufacturerListView(generics.ListAPIView):
    queryset = Manufacturer.objects.all()
    serializer_class = ManufacturerSerializer
    service = ShopService()

    def get(self, request, *args, **kwargs):
        contract_id = self.kwargs['id']
        data = self.service.get_manufacturer(
            request,
            self.queryset,
            contract_id,
        )
        return Response(data=data, status=status.HTTP_200_OK)
