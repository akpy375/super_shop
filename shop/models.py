from django.utils import timezone
from django.db import models


class CreditApplication(models.Model):
    """Credit Application model"""

    created_at = models.DateTimeField(default=timezone.now)


class Contract(models.Model):
    """The contract model"""

    created_at = models.DateTimeField(default=timezone.now)
    credit_application = models.OneToOneField(
        CreditApplication,
        on_delete=models.CASCADE,
        related_name="contract"
    )


class Manufacturer(models.Model):
    """The manufacturer model"""

    created_at = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=255)


class Product(models.Model):
    """The product model"""

    created_at = models.DateTimeField(default=timezone.now)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    credit_application = models.ForeignKey(
        CreditApplication,
        on_delete=models.CASCADE,
        related_name="products"
    )
