from shop.models import Manufacturer


class ShopService:

    def get_manufacturer(self, request, queryset, contract_id: int):

        data = Manufacturer.objects.select_related(
            "contract",
            "product",
            "credit_application",
        ).filter(product__credit_application_id__contract__id=contract_id
                 ).values_list('id', flat=True).distinct()

        return data
