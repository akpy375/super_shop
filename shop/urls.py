from django.urls import path

from shop.views import ManufacturerListView

urlpatterns = [
    path("contract/<int:id>/", ManufacturerListView.as_view(),
         name="contract_list"),
]
